-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2021 at 11:09 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kirjakuy`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `cp` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id`, `nama`, `username`, `alamat`, `cp`, `email`, `password`, `role`) VALUES
(6, 'Sabila Hayati', 'sabilahnr', 'Dusun III, Makamhaji, Kartasura, Sukoharjo Regency, Central Java 57161', '082225134723', 'sabil@mail.com', '$2y$10$nkUeZfuoK2GpPg.JRAC.mediyOPZ1DGZBkWnosYL2gDwkHG3gWhdG', 'customer'),
(7, 'Salsabila Qurrotul', 'salsaqa', '', '', 'salsaqa@mail.com', '$2y$10$8Sb5iF9JUC20JxFwcfj.AeOf08TGNwTFO3aRRNnEnlONf5agUVN9S', 'admin'),
(11, 'Andi Jaya', 'kurir', 'Karanganyar', '088976543211', 'andii@mail.com', '$2y$10$dVQ5QXjAMe/tzjsW.lD0UOBvGv8wdp0HRkVVdAGdLST.tcmkBC0TG', 'kurir'),
(12, 'Syi\'ta Al Mar\'atush Sholihah', 'tahasa', 'Kabayanan II, Kragilan, Gemolong, Sragen Regency, Central Java 57274', '081225967157', 'syitatahasa@mail.com', '$2y$10$NXHtLqREhQTlygJSWs3/cOws09Wf6CmCBdzsd3zWSPOoKPlb703QG', 'customer'),
(18, 'Customer', 'customer', 'Solo', '081227890212', 'cst@mail.com', '$2y$10$tl4Yh.x3HFSTQbhhi0MoVem4O3gm5OSi3dbspn9IyKG2Slelm4lpO', 'customer');

-- --------------------------------------------------------

--
-- Table structure for table `kurir`
--

CREATE TABLE `kurir` (
  `id_kurir` int(11) NOT NULL,
  `id_akun` int(11) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `bagian` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id_status` int(11) NOT NULL,
  `no_pengiriman` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `tgl_sampai` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `nama_penerima` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(12) NOT NULL,
  `tgl_transaksi` varchar(50) NOT NULL DEFAULT current_timestamp(),
  `jenis_paket` varchar(255) NOT NULL,
  `nama_pengirim` varchar(255) NOT NULL,
  `alamat_pengirim` text NOT NULL,
  `cp_pengirim` varchar(30) NOT NULL,
  `nama_tujuan` varchar(255) NOT NULL,
  `alamat_tujuan` text NOT NULL,
  `kota_tujuan` varchar(255) NOT NULL,
  `cp_tujuan` varchar(30) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `jenis_barang` varchar(100) NOT NULL,
  `berat` varchar(100) NOT NULL,
  `stat` varchar(255) NOT NULL,
  `tgl_sampai` varchar(255) NOT NULL,
  `nama_penerima` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `tgl_transaksi`, `jenis_paket`, `nama_pengirim`, `alamat_pengirim`, `cp_pengirim`, `nama_tujuan`, `alamat_tujuan`, `kota_tujuan`, `cp_tujuan`, `nama_barang`, `jenis_barang`, `berat`, `stat`, `tgl_sampai`, `nama_penerima`) VALUES
(5, '08-06-2021 02:16', 'Reguler', 'Adibah', 'Jl. Gatot Subroto No.27 - 28, Kemlayan, Kec. Serengan, Kota Surakarta, Jawa Tengah 57152', '081112345678', 'Sukacitra', 'Jl. Raya Solo - Yogyakarta, Kuncen, Delanggu, Kec. Delanggu, Kabupaten Klaten, Jawa Tengah 57471', 'Klaten', '081329606915', 'SIMSING H08', 'Elektronik', '500', 'Paket telah tiba di tempat tujuan', '2021-06-16', 'Bela'),
(8, '08-06-2021 22:38', 'Reguler', 'Salsa', 'Solo', '123456789', 'Ais', 'Yogya', 'Yogyakarta', '76543281', 'Roti Kecil', 'Makanan', '0', '', '2021-06-16', 'Sabila'),
(9, '08-06-2021 22:47', 'Reguler', 'Syi\'ta Al Mar\'atush Sholihah', 'Jl. Gatot Subroto No.27 - 28, Kemlayan, Kec. Serengan, Kota Surakarta, Jawa Tengah 57152', '081112345678', 'Sabila Hayati N. R.', 'Jl. Raya Solo - Yogyakarta, Kuncen, Delanggu, Kec. Delanggu, Kabupaten Klaten, Jawa Tengah 57471', 'Klaten', '081329606915', 'SIMSING H08', '3', '0.02', '', '', ''),
(12, '16-06-2021 22:14', 'Reguler', 'Salsabita', 'Jl. Gatot Subroto No.27 - 28, Kemlayan, Kec. Serengan, Kota Surakarta, Jawa Tengah 57152', '081112345678', 'Aisa', 'Jl. Raya Solo - Yogyakarta, Kuncen, Delanggu, Kec. Delanggu, Kabupaten Klaten, Jawa Tengah 57471', 'Klaten', '081165786543', 'SOMSONG 08', 'Aksesoris', '700', 'Paket telah tiba di tempat tujuan', '', ''),
(13, '17-06-2021 10:31', 'Kilat', 'Sabila Hayati', 'Jl. Gatot Subroto No.27 - 28, Kemlayan, Kec. Serengan, Kota Surakarta, Jawa Tengah 57152', '081231503118', 'Sukacitra', 'Jl. Raya Solo - Yogyakarta, Kuncen, Delanggu, Kec. Delanggu, Kabupaten Klaten, Jawa Tengah 57471', 'Klaten', '081165786543', 'Roti Kecil', 'Bahan Pokok', '3', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kurir`
--
ALTER TABLE `kurir`
  ADD PRIMARY KEY (`id_kurir`),
  ADD KEY `kurir_ibfk_1` (`id_akun`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`),
  ADD KEY `status_ibfk_2` (`no_pengiriman`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `kurir`
--
ALTER TABLE `kurir`
  MODIFY `id_kurir` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kurir`
--
ALTER TABLE `kurir`
  ADD CONSTRAINT `kurir_ibfk_1` FOREIGN KEY (`id_akun`) REFERENCES `akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `status`
--
ALTER TABLE `status`
  ADD CONSTRAINT `status_ibfk_1` FOREIGN KEY (`no_pengiriman`) REFERENCES `transaksi2` (`no_pengiriman`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `status_ibfk_2` FOREIGN KEY (`no_pengiriman`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
